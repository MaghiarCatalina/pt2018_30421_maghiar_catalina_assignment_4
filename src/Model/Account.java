package Model;

import java.io.Serializable;

public abstract class Account implements Serializable {
    public String name;
    public int money;
    public int id;
    public Account(int id,String name, int money){
        this.id= id;
        this.name=name;
        this.money=money;
    }
    public Account(){
        this.money=0;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
