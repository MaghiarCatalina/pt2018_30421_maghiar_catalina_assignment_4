package Model;

public class SavingAccount extends Account {
    public int dobanda;
    public SavingAccount(){
        this.money=0;
        this.dobanda=5;
    }
    public SavingAccount(int id,String name){
        this.money=0;
        this.id= id;
        this.name= name;
        this.dobanda=5;
    }

    public void afterWithdrawalMoney(){
        this.money = 0;
    }

    public void depositMoney(int sum){
        this.money = sum+dobanda/100*sum;
    }

    @Override
    public String toString() {
        return "id and name:"+this.id+" "+this.name+" ";
    }

}
