package Model;
import View.View;
import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Bank implements BankProc,Serializable {

    private static final long serialVersionUID = -4461406574852904846L;
    public Map<Person,ArrayList<Account>> bank = new HashMap<>();
    public Bank(){}
    public Bank(Map<Person,ArrayList<Account>> bank ){
        this.bank = bank;
    }

    public void addPerson(Person person){
        ArrayList<Account> accounts = new ArrayList<Account>();
        bank.put(person,accounts);
    }
    public void editPerson(Person p,String name,String email){
        //we can't modify the person's id. It is used like a cnp, one person has a single, uneditable one
        p.name=name;
        p.email=email;
    }

    public void addAccount(int personId, int typeAcc, String accName, int accID) {
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == personId) {
                if(typeAcc==1){
                    SavingAccount savingAccount = new SavingAccount(accID,accName);
                    entry.getValue().add(savingAccount);
                    //System.out.println(entry.getValue().get(0).toString());
                }
                else if(typeAcc==2){
                    SpendingAccount spendingAccount = new SpendingAccount(accID,accName);
                    entry.getValue().add(spendingAccount);
                }
                else {System.out.println("Error account type!");}
            }
        }
    }

    public void editAccount(int personID, int accountID, String accname, int accmoney){
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == personID) {
                for(Account aux: entry.getValue()){
                    if(aux.id==accountID){
                        aux.setName(accname);
                        aux.setMoney(accmoney);
                    }
                }
            }
        }
    }

    public void deleteAccount(int personID, int accountID){
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == personID) {
                for (Account aux : entry.getValue()) {
                    //Account aux2 = aux;
                    if (aux.id == accountID) {
                        entry.getValue().remove(aux);
                        break;
                    }
                }
            }
        }
    }
    public JTable viewAllAccounts(int idperson){
        Person person = new Person();
        ArrayList<Account> arrL = new ArrayList<>();
        int i=0;
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()){
            if(entry.getKey().id==idperson){
                person=entry.getKey();
                //System.out.println(person.id+"  "+person.name);
                arrL = entry.getValue();
                if(arrL.isEmpty()){System.out.println("list empty");}
            }
        }
        String data[][]= new String[arrL.size()][3];
        for(int j=0;j<arrL.size();j++){
            //System.out.println("id: "+arrL.get(j).id+" name: "+arrL.get(j).name+" money: "+arrL.get(j).money);
            data[j][0]=arrL.get(j).id+"";
            data[j][1]=arrL.get(j).name;
            data[j][2]=arrL.get(j).money+"";
        }

        String header[]={"ID","Name","Money"};
        JTable jt2 = new JTable(data, header);
        return jt2;
    }

    public void visibility(int persID, int accID,View v) {
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == persID) {
                for (Account aux : entry.getValue()) {
                    if (aux.id == accID) {
                        Class c = aux.getClass();
                        if(c==SpendingAccount.class){
                            v.deposit.setVisible(true);
                            v.depositTF.setVisible(true);
                            v.withdrawal.setVisible(true);
                            v.withdrawalTF.setVisible(true);
                            //aux.money+=sum;
                        }
                        else if(c==SavingAccount.class){
                            if(aux.money==0){
                                v.withdrawalTF.setVisible(false);
                                v.withdrawal.setVisible(false);
                                v.depositTF.setVisible(true);
                                v.deposit.setVisible(true);
                                //aux.setMoney(sum);
                            }
                            else{
                                v.deposit.setVisible(false);
                                v.depositTF.setVisible(false);
                                v.withdrawal.setVisible(true);
                                v.withdrawalTF.setVisible(true);
                                v.withdrawalTF.setText(aux.money+"");
                            }
                        }
                    }
                }
            }
        }
    }
    public void deposit(int persID, int accID, int sum){
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == persID) {
                for (Account aux : entry.getValue()) {
                    if (aux.id == accID) {
                        Class c = aux.getClass();
                        if(c==SpendingAccount.class){
                            aux.money+=sum;
                        }
                        else if(c==SavingAccount.class){
                            if(aux.money==0){
                                aux.setMoney(sum);
                            }
                        }
                    }
                }
            }
        }
    }
    public void withdrawal(int persID, int accID, int sum) {
        for(Map.Entry<Person,ArrayList<Account>> entry: bank.entrySet()) {
            if (entry.getKey().id == persID) {
                for (Account aux : entry.getValue()) {
                    if (aux.id == accID) {
                        Class c = aux.getClass();
                        if(c==SpendingAccount.class){
                            aux.money-=sum;
                        }
                        else if(c==SavingAccount.class){
                            if(aux.money!=0){
                                aux.setMoney(0);
                            }
                        }
                    }
                }
            }
        }

    }
}
