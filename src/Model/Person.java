package Model;

import java.io.Serializable;

public class Person implements Serializable {
    public String name;
    public int id;
    public String email;
    public Person(){}
    public Person(int id,String name, String email){
        this.id = id;
        this.name = name;
        this.email=email;
    }

    @Override
    public int hashCode() {
        int hash=7;
        hash = 31*hash +id;
        hash = 31*hash + (name==null?0: name.hashCode());
        hash = 31*hash + (email==null?0: email.hashCode());
        //System.out.println("The hash code is:"+hash);
        return hash;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.id+"  "+this.name+"  "+this.email;
    }
}
