package Model;
import View.View;
import javax.swing.*;

public interface BankProc {
//declare the methods in bank
    public void addPerson(Person person);
    public void editPerson(Person person,String name, String email);
    //public void deletePerson(Person p);
   // public void viewAllPersons();

    public void addAccount(int personId, int typeAcc, String accName,int accID);
    public void editAccount(int personID, int accountID, String accname, int accmoney);
    public void deleteAccount(int persID, int accID);
   public JTable viewAllAccounts(int idperson);

   public void deposit(int persID, int accID,int sum);
   public void withdrawal(int persID, int accID,int sum);

}
