package View;

import Model.Account;
import Model.Person;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class View {
    public JFrame mainframe = new JFrame();
    private JPanel person = new JPanel();
    private JPanel account = new JPanel();
    private JPanel accountOperations = new JPanel();
    private JTabbedPane tabs = new JTabbedPane();
    public Button addPerson = new Button("+");
    public Button editPerson = new Button("edit");
    public Button deletePerson = new Button("delete");
    public Button viewAllPerson = new Button("view all");
    public Button exitPerson = new Button("exit");
    public Button addAcc = new Button("+");
    public Button editAcc = new Button("edit");
    public Button deleteAcc = new Button("delete");
    public Button viewAllAcc = new Button("view all");
    public Button exitAcc = new Button("exit");
    //Persons
    public TextField newPersId = new TextField();
    public TextField newPersName = new TextField();
    public TextField newPersEmail = new TextField();
    private JLabel newPersIdLabel = new JLabel("ID: ");
    private JLabel newPersNameJlabel = new JLabel("Name: ");
    private JLabel newPersEmailJlabel = new JLabel("Email: ");
    public TextField editPersId = new TextField();
    public TextField editPersName = new TextField();
    public TextField editPersEmail = new TextField();
    private JLabel editPersIdLabel = new JLabel("ID: ");
    private JLabel editPersNameJlabel = new JLabel("New Name: ");
    private JLabel editPersEmailJlabel = new JLabel("New Email: ");
    public TextField deletePersID = new TextField();
    private JLabel deletePersIdLabel = new JLabel("ID: ");
    //Accounts
    public Button exitAccOP = new Button("exit");
    private JLabel addAccLabel = new JLabel("Person ID: ");
    private JLabel addAccLabel2 = new JLabel("Type 1 saving/2 spending:  ");
    private JLabel addAccLabel3 = new JLabel("Account name: ");
    private JLabel addAccLabel4 =  new JLabel("ID Account:");
    public TextField addAccPersonID = new TextField();
    public TextField addAccType = new TextField();
    public TextField addAccName = new TextField();
    public TextField addAccId = new TextField();

    private JLabel editAccLabel1 = new JLabel("Person's id: ");
    public TextField editAccPersonID = new TextField();
    private JLabel editAccLabel2 = new JLabel("Account's id: ");
    public TextField editAccID = new TextField();
    private JLabel editAccLabel3 = new JLabel("New account name: ");
    public TextField editAccNewName = new TextField();
    private JLabel editAccLabel4 = new JLabel("New account sum: ");
    public TextField editAccNewMoney = new TextField();

    private JLabel deleteAccPersIDLabel = new JLabel("Person ID: ");
    public TextField deleteAccPersID = new TextField();
    private JLabel deleteAccAccIDLabel = new JLabel("Account ID: ");
    public TextField deleteAccAccID = new TextField();

    private JLabel viewAllAccPersLabel = new JLabel("Person ID: ");
    public TextField viewAllAccPers = new TextField();

    //Account Operations
    private JLabel opPersIDLabel = new JLabel("Person ID: ");
    public TextField opPersId = new TextField();
    private JLabel opAccIDLabel = new JLabel("Account ID: ");
    public TextField opAccId = new TextField();
    public JButton deposit = new JButton("Deposit");
    public TextField depositTF = new TextField();
    public JButton withdrawal = new JButton("Withdrawal");
    public TextField withdrawalTF = new TextField();
    public JButton next = new JButton("next");
    public View(){
        mainframe.add(person);
        mainframe.add(account);
        mainframe.add(accountOperations);
        tabs.setBounds(0,0,500,500);
        tabs.add("Persons",person);
        tabs.add("Accounts",account);
        tabs.add("Account Operations",accountOperations);
        mainframe.add(tabs);
        mainframe.setTitle("Bank");
        mainframe.setSize(500,500);
        mainframe.setVisible(true);
        person.add(addPerson);
        person.add(newPersIdLabel);
        person.add(newPersId);
        person.add(newPersNameJlabel);
        person.add(newPersName);
        person.add(newPersEmailJlabel);
        person.add(newPersEmail);
        person.add(editPerson);
        person.add(editPersIdLabel);
        person.add(editPersId);
        person.add(editPersNameJlabel);
        person.add(editPersName);
        person.add(editPersEmailJlabel);
        person.add(editPersEmail);
        person.add(deletePerson);
        person.add(deletePersIdLabel);
        person.add(deletePersID);
        person.add(viewAllPerson);
        person.add(exitPerson);

        account.add(addAcc);
        account.add(addAccLabel);
        account.add(addAccPersonID);
        account.add(addAccLabel2);
        account.add(addAccType);
        account.add(addAccLabel3);
        account.add(addAccName);
        account.add(addAccLabel4);
        account.add(addAccId);

        account.add(editAcc);
        account.add(editAccLabel1);
        account.add(editAccPersonID);
        account.add(editAccLabel2);
        account.add(editAccID);
        account.add(editAccLabel3);
        account.add(editAccNewName);
        account.add(editAccLabel4);
        account.add(editAccNewMoney);

        account.add(deleteAcc);
        account.add(deleteAccPersIDLabel);
        account.add(deleteAccPersID);
        account.add(deleteAccAccIDLabel);
        account.add(deleteAccAccID);

        account.add(viewAllAcc);
        account.add(viewAllAccPersLabel);
        account.add(viewAllAccPers);
        account.add(exitAcc);

        accountOperations.add(opPersIDLabel);
        accountOperations.add(opPersId);
        accountOperations.add(opAccIDLabel);
        accountOperations.add(opAccId);
        accountOperations.add(next);
        accountOperations.add(deposit);
        accountOperations.add(depositTF);
        accountOperations.add(withdrawal);
        accountOperations.add(withdrawalTF);
        deposit.setVisible(false);
        depositTF.setVisible(false);
        withdrawal.setVisible(false);
        withdrawalTF.setVisible(false);
        accountOperations.add(exitAccOP);
    }
}
