package Controller;

import Model.*;
import View.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Map;

public class Controller {
    public Bank mybank = new Bank();
    Person person1 = new Person(1,"Ana","ana@yahoo.com");
   // Person person2 = new Person(2,"Andrei","andrei@yahoo.com");
    ArrayList<Account> pers1accounts = new ArrayList<Account>();
    //ArrayList<Account> pers2accounts = new ArrayList<Account>();
    //SavingAccount savingAccountP1 = new SavingAccount(5);
    //SpendingAccount spendingAccountP2 = new SpendingAccount();

    public Controller(View v) {
        mybank.bank.put(person1,pers1accounts);
        //mybank.bank.put(person2,pers2accounts);
        //pers1accounts.add(savingAccountP1);
       // pers2accounts.add(spendingAccountP2);
        try{

            FileInputStream filein = new FileInputStream("banca.ser");
            ObjectInputStream in = new ObjectInputStream(filein);
            mybank=(Bank)in.readObject();
            in.close();
            filein.close();
        }
        catch(Exception firstexception){firstexception.printStackTrace();}

        v.exitPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        v.exitAcc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        v.exitAccOP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        v.addPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    Person person = new Person(Integer.parseInt(v.newPersId.getText()), v.newPersName.getText(), v.newPersEmail.getText());
                    mybank.addPerson(person);
                    System.out.println("New person added");
                    try{
                        FileOutputStream fileout = new FileOutputStream("banca.ser");
                        ObjectOutputStream out = new ObjectOutputStream(fileout);
                        out.writeObject(mybank);
                        out.close();
                        fileout.close();
                    }
                catch (Exception e1){e1.printStackTrace();}
            }
        });

        v.editPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               int persid = Integer.parseInt(v.editPersId.getText());
               for(Person key: mybank.bank.keySet()){
                   if(key.id==persid){
                       mybank.editPerson(key,v.editPersName.getText(),v.editPersEmail.getText());
                   }
               }
               System.out.println("Person edited");
               try{
                   FileOutputStream fileout = new FileOutputStream("banca.ser");
                   ObjectOutputStream out = new ObjectOutputStream(fileout);
                   out.writeObject(mybank);
                   out.close();
                   fileout.close();
               }
               catch(Exception e1){e1.printStackTrace();}
            }
        });

        v.deletePerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int delid = Integer.parseInt(v.deletePersID.getText());
                for(Person key: mybank.bank.keySet()){
                    if(key.id==delid){
                        mybank.bank.remove(key);
                        System.out.println("Person deleted");
                    }
                }
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });

        v.viewAllPerson.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data[][]= new String[mybank.bank.size()][3];
                int i=0;
                for (Person key : mybank.bank.keySet()) {
                        data[i][0]=key.id+"";
                        data[i][1]=key.name;
                        data[i][2]=key.email;
                        i++;
                }
                String header[]={"ID","Name","Email"};
                JTable jt = new JTable(data, header);
                Frame ftable = new Frame();
                ftable.setVisible(true);
                ftable.setSize(300, 300);
                jt.setAutoscrolls(true);
                jt.setEnabled(true);
                JScrollPane sp = new JScrollPane(jt);
                ftable.add(sp);
            }
        });

        v.addAcc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mybank.addAccount(Integer.parseInt(v.addAccPersonID.getText()),Integer.parseInt(v.addAccType.getText()),v.addAccName.getText(),Integer.parseInt(v.addAccId.getText()));
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });

        v.editAcc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mybank.editAccount(Integer.parseInt(v.editAccPersonID.getText()),Integer.parseInt(v.editAccID.getText()),v.editAccNewName.getText(),Integer.parseInt(v.editAccNewMoney.getText()));
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });

        v.deleteAcc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mybank.deleteAccount(Integer.parseInt(v.deleteAccPersID.getText()),Integer.parseInt(v.deleteAccAccID.getText()));
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });
        v.viewAllAcc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTable jTable = new JTable() ;
                jTable = mybank.viewAllAccounts(Integer.parseInt(v.viewAllAccPers.getText()));
                Frame ftable2 = new Frame();
                ftable2.setVisible(true);
                ftable2.setSize(300, 300);
                jTable.setAutoscrolls(true);
                jTable.setEnabled(true);
                JScrollPane sp2 = new JScrollPane(jTable);
                ftable2.add(sp2);

            }
        });
        v.next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mybank.visibility(Integer.parseInt(v.opPersId.getText()),Integer.parseInt(v.opAccId.getText()),v);
            }
        });
        v.deposit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            mybank.deposit(Integer.parseInt(v.opPersId.getText()),Integer.parseInt(v.opAccId.getText()),Integer.parseInt(v.depositTF.getText()));
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });
        v.withdrawal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mybank.withdrawal(Integer.parseInt(v.opPersId.getText()),Integer.parseInt(v.opAccId.getText()),Integer.parseInt(v.withdrawalTF.getText()));
                try{
                    FileOutputStream fileout = new FileOutputStream("banca.ser");
                    ObjectOutputStream out = new ObjectOutputStream(fileout);
                    out.writeObject(mybank);
                    out.close();
                    fileout.close();
                }
                catch(Exception e2){e2.printStackTrace();}
            }
        });
    }
}
