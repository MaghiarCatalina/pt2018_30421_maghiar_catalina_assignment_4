package Controller;

import View.View;

public class Start {
    public static void main(String args[]){
        View view = new View();
        Controller c = new Controller(view);
    }
}
